#ifndef KNOTVECTOR_H
#define KNOTVECTOR_H

#include <parametrics/gmpsurf>

using namespace GMlib;

class Knotvector: public DVector<float>  {
public:
  Knotvector(){}
  Knotvector(float s, float e, int n, bool close = false);
  ~Knotvector();


};

#endif
