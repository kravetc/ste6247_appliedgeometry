#include "MyERBS.h"

MyERBS::MyERBS(PSurf<float, 3> *original, int _n1, int _n2, int _d1, int _d2, int type){

    alpha = 30;
    x0 = 0;

    setValues(original, _n1, _n2, _d1, _d2, type);

    closedU = original->isClosedU();
    closedV = original->isClosedV();

    if(closedU) _nU++;
    if(closedV) _nV++;

    _knotU = Knotvector(original->getParStartU(), original->getParEndU(), _nU, closedU);
    _knotV = Knotvector(original->getParStartV(), original->getParEndV(), _nV, closedV);

    //std::cout << _knotU << std::endl;
    //std::cout << _knotV << std::endl;

    if (type == 0) makeLocalSurfaces1();
    else if (type == 1) makeLocalSurfaces2();
    else if (type == 2) makeLocalSurfaces1();
}

MyERBS::~MyERBS(){}

void MyERBS::setValues(PSurf<float, 3> *original, int _n1, int _n2, int _d1, int _d2, int type){
    _original = original;
    _nU = _n1;
    _nV = _n2;
    _dU = _d1;
    _dV = _d2;
    _type = type;
}

void MyERBS::makeLocalSurfaces1(){
  _C.setDim(_nU + 1, _nV + 1);

  for (int i = 0; i < _nU-1; i++)
    for (int j = 0; j < _nV-1; j++){
      _C[i][j] = new SubSurface(_original, _knotU[i], _knotV[j], _knotU[i + 2], _knotV[j + 2], _knotU[i + 1], _knotV[j + 1]);
      //add(_C[i][j]);
    }

  for (int i = 0; i < _nU-1; i++)
    if (_original->isClosedV()){
      _C[i][_nV - 1] = _C[i][0];
    }
    else{
      _C[i][_nV - 1] = new SubSurface(_original, _knotU[i], _knotV[_nV - 1], _knotU[i + 2], _knotV[_nV - 1 + 2], _knotU[i + 1], _knotV[_nV - 1 + 1]);
      //add(_C[i][_nV - 1]);
    }

  for (int j = 0; j < _nV-1; j++)
    if (_original->isClosedU()){
      _C[_nU-1][j] = _C[0][j];
    }
    else{
      _C[_nU-1][j] = new SubSurface(_original, _knotU[_nU - 1], _knotV[j], _knotU[_nU - 1 + 2], _knotV[j + 2], _knotU[_nU - 1 + 1], _knotV[j + 1]);
      //add(_C[_nU-1][j]);
    }

  if (!_original->isClosedU() && !_original->isClosedV()){
      _C[_nU-1][_nV-1] = new SubSurface(_original, _knotU[_nU - 1], _knotV[_nV - 1], _knotU[_nU - 1 + 2], _knotV[_nV - 1 + 2], _knotU[_nU], _knotV[_nV]);
      //add(_C[_nU-1][_nV-1]);
  }
  else if(_original->isClosedU())
      _C[_nU-1][_nV-1] = _C[0][_nV-1];
  else if(_original->isClosedV())
      _C[_nU-1][_nV-1] = _C[_nU-1][0];

  if (_type == 2){
      _C[2][2]->translate(Vector<float,3> (-1.0, -1.0, 2.0));
      _C[2][2]->rotate(45, Vector<float,3> (1.0, -1.0, 0.0));
      add(_C[2][2]);
      _C[3][2]->translate(Vector<float,3> (1.0, -1.0, 2.0));
      _C[3][2]->rotate(45, Vector<float,3> (1.0, 1.0, 0.0));
      add(_C[3][2]);
      _C[2][3]->translate(Vector<float,3> (-1.0, 1.0, 2.0));
      _C[2][3]->rotate(45, Vector<float,3> (-1.0, -1.0, 0.0));
      add(_C[2][3]);
      _C[3][3]->translate(Vector<float,3> (1.0, 1.0, 2.0));
      _C[3][3]->rotate(45, Vector<float,3> (-1.0, 1.0, 0.0));
      add(_C[3][3]);
  }

  if (_type == 0){
      _C[1][1]->translate(Vector<float,3> (-2.0, 0.0, 0.0));
  }

}

void MyERBS::makeLocalSurfaces2(){
    _C.setDim(_nU + 1, _nV + 1);

    for (int i = 0; i < _nU-1; i++)
      for (int j = 0; j < _nV-1; j++){
       _C[i][j] = new BezierSurface(_original, _knotU[i], _knotV[j], _knotU[i+2], _knotV[j+2], _knotU[i+1], _knotV[j+1], _dU, _dV);
       //add(_C[i][j]);
        }

    for (int i = 0; i < _nU-1; i++)
      if (_original->isClosedV())
        _C[i][_nV-1] = _C[i][0];
      else{
       _C[i][_nV-1] = new BezierSurface(_original, _knotU[i], _knotV[_nV-1], _knotU[i+2], _knotV[_nV+1], _knotU[i+1], _knotV[_nV], _dU, _dV);
      // add(_C[i][_nV - 1]);
      }

    for (int j = 0; j < _nV-1; j++)
      if (_original->isClosedU())
        _C[_nU-1][j] = _C[0][j];
      else{
        _C[_nU-1][j] = new BezierSurface(_original, _knotU[_nU-1], _knotV[j], _knotU[_nU+1], _knotV[j+2], _knotU[_nU], _knotV[j+1], _dU, _dV);
       // add(_C[_nU-1][j]);

      }
    if (!_original->isClosedU() && !_original->isClosedV()){
        _C[_nU-1][_nV-1] = new BezierSurface(_original, _knotU[_nU - 1], _knotV[_nV - 1], _knotU[_nU - 1 + 2], _knotV[_nV - 1 + 2], _knotU[_nU], _knotV[_nV], _dU, _dV);
       // add(_C[_nU-1][_nV-1]);
    }
    else if(_original->isClosedU())
        _C[_nU-1][_nV-1] = _C[0][_nV-1];
    else if(_original->isClosedV())
        _C[_nU-1][_nV-1] = _C[_nU-1][0];


    _C[1][1]->translate(Vector<float,3>(0.0, 0.0, 2.0));


    //airplane
    /*_C[2][0]->translate(Vector<float,3> (0.0, -2.0, -0.5));

    _C[0][0]->translate(Vector<float,3> (-1.0, 1.0, -0.5));
    _C[4][0]->translate(Vector<float,3> (1.0, 1.0, -0.5));

    _C[0][1]->translate(Vector<float,3> (-2.0, 1.0, -1.0));
    _C[4][1]->translate(Vector<float,3> (2.0, 1.0, -1.0));

    _C[0][2]->translate(Vector<float,3> (-1.0, 1.0, -0.5));
    _C[4][2]->translate(Vector<float,3> (1.0, 1.0, -0.5));

    _C[0][4]->translate(Vector<float,3> (1.0, -1.0, -0.2));
    _C[4][4]->translate(Vector<float,3> (-1.0, -1.0, -0.2));


    _C[2][2]->translate(Vector<float,3> (0.0, 0.0, 0.5));
    _C[2][3]->translate(Vector<float,3> (0.0, 0.0, 1.0));
    _C[2][4]->translate(Vector<float,3> (0.0, 0.0, 1.5));


    _C[1][0]->translate(Vector<float,3> (0.5, 0.0, -0.2));
    _C[3][0]->translate(Vector<float,3> (-0.5, 0.0, -0.2));*/
}


float MyERBS::getStartPU(){
    return _original->getParStartU();
}

float MyERBS::getStartPV(){
    return _original->getParStartV();
}

float MyERBS::getEndPU(){
    return _original->getParEndU();
}

float MyERBS::getEndPV(){
    return _original->getParEndV();
}

bool MyERBS::isClosedU() const{
    return _original->isClosedU();
}

bool MyERBS::isClosedV() const{
    return _original->isClosedV();
}

void MyERBS::eval(float u, float v, int d1, int d2, bool u1, bool u2){
    //find index i and j
    int indexI = findIndex(u, _knotU, _nU, _original->isClosedU());
    int indexJ = findIndex(v, _knotV, _nV, _original->isClosedV());
    //std::cout << indexI << " " << indexJ << std::endl;


    DMatrix<Vector<float,3> > S00, S, Su, Sv;
    S00.setDim(2,2);
    S.setDim(2,2);
    Su.setDim(2,2);
    Sv.setDim(2,2);
    for(int i = 0; i < 2; i++)
        for (int j = 0; j < 2; j++){
            S00 = _C[indexI-1+i][indexJ-1+j]->evaluateParent(u , v, 1, 1);
           // std::cout << S00 << std::endl;
            S[i][j] = S00[0][0];
            Su[i][j] = S00[0][1];
            Sv[i][j] = S00[1][0];
        }

    S.transpose();
    Su.transpose();
    Sv.transpose();

    //std::cout << Sv << std::endl;


     DVector<float> Bu(2), Bud(2);

     _B1.set(_knotU[indexI], _knotU[indexI+1] - _knotU[indexI]);

     Bu[0] = 1 - _B1(u);
     Bud[1] = _B1.getDer1();
     Bud[0] = -Bud[1];
     Bu[1] =  _B1(u);

     DVector<float> Bv(2), Bvd(2);

     _B2.set(_knotV[indexJ], _knotV[indexJ+1] - _knotV[indexJ]);

     Bv[0] = 1 - _B2(v);
     Bvd[1] = _B2.getDer1();
     Bvd[0] = -Bvd[1];
     Bv[1] =  _B2(v);

     //std::cout << Bu[0]*Bv[0] + Bu[0]*Bv[1] + Bu[1]*Bv[0] + Bu[1]*Bv[1] << std::endl;

     _p.setDim(3,3);

     _p[0][0] = Bv*(S^Bu);
     _p[0][1] = Bv*(S^Bud) + Bv*(Su^Bu);
     _p[1][0] = Bvd*(S^Bu) + Bv*(Sv^Bu);
    // std::cout << _p << std::endl;

}

void MyERBS::localSimulate(double dt){
    if(_type == 0){
        //_C[2][1]->translate((Vector<float,3> (0.0,0.0,sin(x0)/50.0)));
        //_C[1][1]->translate((Vector<float,3> (0.0,0.0,sin(x0)/50.0)));
        //x0 += 0.02;
        //if (x0 > 2*M_PI) x0 = 0;
        _C[1][1]->rotate(dt*alpha, Vector<float,3> (1.0,0.0,0.0));


    }
    if (_type == 1){
        _C[1][1]->rotate(dt*alpha, Vector<float,3> (0.0,0.0,0.1));
    }
    if (_type == 2){
        _C[2][2]->rotate(dt*alpha, Point<float,3> (2.0,2.0,0.0), Vector<float,3> (0.0,0.0,1.0));
        _C[2][3]->rotate(dt*alpha, Point<float,3> (2.0,2.0,0.0), Vector<float,3> (0.0,0.0,1.0));
        _C[3][2]->rotate(dt*alpha, Point<float,3> (2.0,2.0,0.0), Vector<float,3> (0.0,0.0,1.0));
        _C[3][3]->rotate(dt*alpha, Point<float,3> (2.0,2.0,0.0), Vector<float,3> (0.0,0.0,1.0));

    }
    replot();
}

int MyERBS::findIndex(float dir, Knotvector k, int n, bool closed){
    int index = 0;

    if(closed) n--;

    for (index = 1; index < n-1; index++)
        if (dir < k[index+1])
            break;

    if(closed)
        while(std::abs(k[index+1] - k[index]) < EPSILON)
            index--;

    return index;
}


void MyERBS::add(PSurf<float, 3> *s){
    s->enableDefaultVisualizer();
    s->replot(10,10,1,1);
    insert(s);
}
