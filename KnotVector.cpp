#include "KnotVector.h"

Knotvector::Knotvector(float s, float e, int n, bool close)
{
    int k = 2;

    this->setDim(n + k);

    for (int i = 0; i < k; i++)
        (*this)[i] = s;

    float dt = (e - s)/(n - 1);

    for (int i = k; i < n; i++)
        (*this)[i] = s + (i - k + 1)*dt;


    for (int i = 0; i < k; i++)
        (*this)[n + i] = e;

    if (close){
        (*this)[0] = (*this)[1] - ((*this)[n] - (*this)[n-1]);
        (*this)[n+1] = (*this)[n] + ((*this)[2] - (*this)[1]);
    }
}

Knotvector::~Knotvector(){}

