#pragma once

#include <gmParametricsModule>

#include "physicalobject.h"
//#include "plane.h"
//#include "wall.h"

class Ball: public PhysicalObject, public GMlib::PSphere<float>
{
public:
    Ball(const GMlib::Point<float,3>& position,
         const GMlib::Vector<float,3>& velocity,
         float radius,
         float mass);

    GMlib::Vector<float, 3> velocity;
    float mass;

    GMlib::Point<float,3> position() const;
    float radius() const;
    
    bool rolling = false;

    //visitors
    bool collide(const PhysicalObject* po, float& t) const override;
    void impact(PhysicalObject* po) override;

    //static bool collide(const Ball& ball1, const Ball& ball2, float& time);

    void update(float dt) override;


    bool collideV(const Ball* ball, float& t) const override;
    //bool collideV(const Plane* plane, float& t) const override;
    //bool collideV(const Wall* wall, float& t) const override;
    void impactV(Ball* ball) override;
    //void impactV(Plane* plane) override;
   // void impactV(Wall* wall) override;

    virtual void applyForce(GMlib::Vector<float, 3> force, float dt) override;


private:

};
