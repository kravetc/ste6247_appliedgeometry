#ifndef SUBSURFACE_H
#define SUBSURFACE_H

#include <parametrics/gmpsurf>
#include <gmSceneModule>
#include <gmParametricsModule>

using namespace GMlib;

class SubSurface: public PSurf<float, 3> {
    GM_SCENEOBJECT(SubSurface)
public:
  SubSurface(PSurf<float,3>* original, float u1, float v1,  float u2, float v2, float u, float v);
  ~SubSurface();

protected:
  PSurf<float,3> *_c;
  float _u1;
  float _v1;
  float _u2;
  float _v2;
  Vector<float,3> _move;

  bool isClosedU() const;
  bool isClosedV() const;

  void eval(float u, float v, int d1, int d2, bool u1, bool u2);
  float getStartPU();
  float getStartPV();
  float getEndPU();
  float getEndPV();

private:
  void setValues(PSurf<float,3>* original, float u1, float v1,  float u2, float v2);
};

#endif
