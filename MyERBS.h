#ifndef MYERBS_H
#define MYERBS_H

#include "SubSurface.h" 
#include "BezierSurface.h"
#include "KnotVector.h"

#include <gmSceneModule>
#include <gmParametricsModule>

#define EPSILON 1e-5

using namespace GMlib;

class MyERBS: public PSurf<float, 3>{
    GM_SCENEOBJECT(MyERBS)
public:
    MyERBS(PSurf<float,3>* original, int _n1, int _n2, int _d1, int _d2, int type);
    ~MyERBS();

protected:

    void eval(float u, float v, int d1, int d2, bool u1, bool u2);
    float getStartPU();
    float getStartPV();
    float getEndPU();
    float getEndPV();
    bool isClosedU() const;
    bool isClosedV() const;
    void localSimulate(double dt);

private:
  void makeLocalSurfaces1(); //sub surface
  void makeLocalSurfaces2(); //Bezier surface

  int findIndex(float dir, Knotvector k, int n, bool closed);
  void add(PSurf<float,3> *s);

  void setValues(PSurf<float,3>* original, int _n1, int _n2, int _d1, int _d2, int type);

  Vector<float,3> _U, _V;
  bool closedU, closedV;

  Knotvector _knotU;
  Knotvector _knotV;

  PSurf<float,3>* _original;
  int _type;
  int _dU;
  int _dV;
  int _nU;
  int _nV;

  Angle alpha;
  float x0;


  DMatrix<PSurf<float,3>* > _C; //sub surfaces
  ERBSEvaluator<double> _B1; //evaluator1
  ERBSEvaluator<double> _B2; //evaluator2


};

#endif
