#include "BezierSurface.h"

#include <gmParametricsModule>
#include <QtCore>

BezierSurface::BezierSurface(PSurf<float, 3> *original, float u1, float v1, float u2, float v2, float u, float v, float d1, float d2){
    _c.setDim(d1+1, d2+1);
    _o = original;

    setValues(u1, v1, u2, v2, u, v, d1, d2);
    cntrlPoints(original, u, v, d1, d2);
   // std::cout << _c << std::endl;
}

BezierSurface::~BezierSurface(){

}

DMatrix<float>& BezierSurface::BMatrix(int d, float t, float gamma){
    static DMatrix<float> m;

    m.setDim(d+1, d+1);

    if (d < 1){
        m[0][0] = 1;
        return m;
    }

    m[d-1][0] = 1 - t;
    m[d-1][1] = t;
    for (int i = d-2; i >= 0; i--){
      m[i][0] = (1-t)*m[i+1][0];
      for (int j = 1; j < d-i; j++)
        m[i][j] = (1-t)*m[i+1][j] + t*m[i+1][j-1];
      m[i][d-i] = t*m[i+1][d-(i+1)];
    }

    m[d][0] = -gamma;
    m[d][1] = gamma;
    for (int k = 2; k <= d; k++){
      float s = k*gamma;
      for (int i = d; i > d-k; i--){
        m[i][k] = s*m[i][k-1];
        for (int j = k-1; j > 0; j--)
          m[i][j] = s*(m[i][j-1] - m[i][j]);
        m[i][0] *= -s;
      }
    }
    //std::cout << m << std::endl;

    return m;
}

void BezierSurface::setValues(float u1, float v1, float u2, float v2, float u, float v, int d1, int d2){
    Wu = (u - u1)/(u2 - u1);
    Wv = (v - v1)/(v2 - v1);

    deltau = 1.0/(u2 - u1);
    deltav = 1.0/(v2 - v1);

    dim1 = d1;
    dim2 = d2;

}

void BezierSurface::cntrlPoints(PSurf<float,3>* original, float u, float v, int d1, int d2){
    DMatrix<Vector<float,3> >  g;
    g.setDim(d1+1, d2+1);
    g = original->evaluateParent(u, v, 1, 1);
    g[1][1] = Vector<float,3> (float(0));
    //std::cout << g << std::endl;
    DMatrix<float> Bu;
    DMatrix<float> Bv;

    /*std::cout << Wu << std::endl;
    std::cout << Wv << std::endl;
    std::cout << deltau << std::endl;
    std::cout << deltav << std::endl;*/

    Bu = BMatrix(g.getDim1()-1, Wu, deltau);
    Bv = BMatrix(g.getDim2()-1, Wv, deltav);

    Bu.invert();
    Bv.invert();

    Bv.transpose();

    //std::cout << Bu << std::endl;
   // std::cout << Bv << std::endl;
    _c = Bu * (g ^ Bv);

   for(int i = 0; i < _c.getDim1(); i++)
       for(int j = 0; j < _c.getDim2(); j++)
         _c[i][j] -= g[0][0];
    translate( g[0][0] );

    //std::cout << _c << std::endl;

     for(int i = 0; i < _c.getDim1(); i++)
         for(int j = 0; j < _c.getDim2(); j++)
         {
           GMlib::PSphere<float> *s = new GMlib::PSphere<float>(0.05);
           s->translateGlobal(_c[i][j]);
           s->setMaterial(GMmaterial::BlackPlastic);
           s->toggleDefaultVisualizer();
           s->replot(10, 10, 1, 1);
           insert(s);
         }


}

void BezierSurface::eval(float u, float v, int d1, int d2, bool u1, bool u2){
    this->_p.setDim(_c.getDim1(), _c.getDim2());
   // std::cout << u << std::endl;
   // std::cout << v << std::endl;

    DMatrix<float> Bu;
    DMatrix<float> Bv;

    Bu = BMatrix(this->_p.getDim1()-1, u, 1);
    Bv = BMatrix(this->_p.getDim2()-1, v, 1);


    Bv.transpose();
    this->_p = Bu * (_c ^ Bv);

   // std::cout << _p << std::endl;
}


float BezierSurface::getStartPU(){
    return 0.0;
}

float BezierSurface::getStartPV(){
    return 0.0;
}

float BezierSurface::getEndPU(){
    return 1.0;
}

float BezierSurface::getEndPV(){
    return 1.0;
}

bool BezierSurface::isClosedU() const{
    return _o->isClosedU();
}

bool BezierSurface::isClosedV() const{
    return _o->isClosedV();
}
