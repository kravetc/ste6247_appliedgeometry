#include "SubSurface.h"

SubSurface::SubSurface(PSurf<float, 3> *original, float u1, float v1, float u2, float v2, float u, float v){
    setValues(original, u1, v1, u2, v2);

    DMatrix<Vector<float, 3> > move = _c->evaluateParent(u, v, 0, 0);
    _move = move[0][0];
    translate(_move);

}

SubSurface::~SubSurface(){}

bool SubSurface::isClosedU() const{
    return _c->isClosedU();
}

bool SubSurface::isClosedV() const{
    return _c->isClosedV();
}

void SubSurface::eval(float u, float v, int d1, int d2, bool u1, bool u2){
    _p = _c->evaluateParent(u, v, 1, 1);
    _p[0][0] -= _move;
}

float SubSurface::getStartPU(){
    return _u1;
}

float SubSurface::getStartPV(){
    return _v1;
}

float SubSurface::getEndPU(){
    return _u2;
}

float SubSurface::getEndPV(){
    return _v2;
}

void SubSurface::setValues(PSurf<float, 3> *original, float u1, float v1, float u2, float v2){
    _c = original;
    _u1 = u1;
    _v1 = v1;
    _u2 = u2;
    _v2 = v2;

}
