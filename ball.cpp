#include "ball.h"
//#include "collision.h"

constexpr float epsilon = 1e-8;

Ball::Ball(const GMlib::Point<float,3>& position,
     const GMlib::Vector<float,3>& velocity,
     float radius,
     float mass_):
        GMlib::PSphere<float>(radius), velocity(velocity), mass(mass_)
{
    PhysicalObject::dynamic = true;
    this->translate(position);
}

GMlib::Point<float,3> Ball::position() const
{
    //return DisplayObject::getPos();
}

void Ball::update(float dt)
{
    if (rolling && velocity.getLength() > 0){
      auto a = velocity.getLength()/radius()*dt;
      auto v = velocity;
      if (velocity.getLength() > 0.7) v = velocity^GMlib::Vector<float,3>{0.0,0.0,-1.0};
      this->rotate(GMlib::Angle(a), position(), v);
    }
    this->translate(dt*velocity*mass);

    if (rolling && velocity.getLength() < epsilon) velocity = {0,0,0};
}


float Ball::radius() const
{
    return GMlib::PSphere<float>::getRadius();
}

bool Ball::collide(const PhysicalObject* po, float& t) const{
    return po->collideV(this, t);
}

void Ball::impact(PhysicalObject* po) {
    po->impactV(this);
}

/*bool Ball::collideV(const Ball* ball, float& t) const {
    return Collision::collide(ball,this,t);
}

bool Ball::collideV(const Plane* plane, float& t) const {
    return Collision::collide(this,plane,t);
}

bool Ball::collideV(const Wall* wall, float& t) const {
    return Collision::collide(this,wall,t);
}

void Ball::impactV(Ball* ball) {
    ::Collision::impact(ball,this);
}

void Ball::impactV(Plane* plane){
    ::Collision::impact(this,plane);
}

void Ball::impactV(Wall* wall){
    ::Collision::impact(this,wall);
}

void Ball::applyForce(GMlib::Vector<float, 3> force, float dt){
    velocity += force*dt;
}*/

