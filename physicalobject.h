#pragma once

#include <gmSceneModule>

//class Plane;
class Ball;
//class Wall;

class PhysicalObject
{
public:
    bool dynamic = false;
    virtual void update(float dt) = 0;
    virtual void applyForce(GMlib::Vector<float, 3> force, float dt){}

    //visitors
    virtual bool collide(const PhysicalObject* po, float & t) const = 0;
    virtual void impact(PhysicalObject * object) = 0;


    virtual bool collideV(const Ball* ball, float& t) const = 0;
    //virtual bool collideV(const Plane* plane, float& t) const = 0;
    //virtual bool collideV(const Wall* wall, float& t) const = 0;
    virtual void impactV(Ball* ball) = 0;
    //virtual void impactV(Plane* plane) = 0;
    //virtual void impactV(Wall* wall) = 0;
};
