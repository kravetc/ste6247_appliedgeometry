#ifndef BEZIERSURFACE_H
#define BEZIERSURFACE_H

#include <parametrics/gmpsurf>
#include <gmSceneModule>
#include <gmParametricsModule>

using namespace GMlib;

class BezierSurface: public PSurf<float, 3> {
    GM_SCENEOBJECT(BezierSurface)
public:
  BezierSurface(PSurf<float, 3> *original, float u1, float v1, float u2, float v2, float u, float v, float d1, float d2);
  ~BezierSurface();

  float Wu;
  float Wv;

  float deltau;
  float deltav;

  int dim1;
  int dim2;

  PSurf<float,3> *_o;

  DMatrix<Vector<float,3> > _c;


protected:
  bool isClosedU() const;
  bool isClosedV() const;

  void eval(float u, float v, int d1, int d2, bool u1, bool u2);
  float getStartPU();
  float getStartPV();
  float getEndPU();
  float getEndPV();

private:
  void setValues(float u1, float v1, float u2, float v2, float u, float v, int d1, int d2);
  DMatrix<float> &BMatrix(int d, float t, float gamma);
  void cntrlPoints(PSurf<float, 3> *original, float u, float v, int d1, int d2);
};

#endif
